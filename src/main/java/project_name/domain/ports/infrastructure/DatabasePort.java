package project_name.domain.ports.infrastructure;

import project_name.domain.model.ArticuloDOMAIN;

public interface DatabasePort {

    ArticuloDOMAIN getArticuloByName(String name);


}
