#!/bin/bash

source "${BASH_SOURCE%/*}/libs/env"

cd "$SCRIPT_PATH" || exit 1
cd ..

# Jacoco
############

./mvnw clean verify -P jacoco

open -a "Google Chrome" target/site/jacoco/index.html
